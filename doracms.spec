%define name doracms
%define version 1.0.0
%define release 1
%define buildroot %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

Name: %{name}
Version: %{version}
Release: %{release}
Summary: doracms

Group: Installation Script
License: MIT
Source: %{name}.tar.gz
BuildRoot: %{buildroot}
Requires: nodejs >= 10.11.0
Requires: mariadb >= 10.0.0
BuildRequires: nodejs >= 10.11.0
AutoReqProv: no

%description

%global debug_package %{nil}

%prep
%setup -q -c -n %{name}

%build

#It runs right before installation of rpm
%pre
getent group doracms >/dev/null || groupadd -r doracms
getent passwd doracms >/dev/null || useradd -r -g doracms -G doracms -d / -s /sbin/nologin -c "doracms" doracms

#it install rpm package in the build time
%install
mkdir -p %{buildroot}/usr/lib/doracms
cp -r ./ %{buildroot}/usr/lib/doracms
mkdir -p %{buildroot}/var/log/doracms
cd %{buildroot}/usr/lib/doracms


#Post installation
%post
cp /usr/lib/doracms/doracms.service /etc/systemd/system/doracms.service
systemctl daemon-reload
systemctl enable doracms
systemctl restart doracms

#runs on uninstallation or upgrade (uninstall previous version)
%postun
if [ $1 == 0 ] ; then
	#uninstall package
	systemctl stop doracms
	systemctl disable doracms
	rm /etc/systemd/system/doracms.service
fi


%clean
rm -rf %{buildroot}

%files
%defattr(644, doracms, doracms, 755)
/usr/lib/doracms
/var/log/doracms
