# DoraCMS

#### 介绍
`DoraCMS` 是基于 `Nodejs` `eggjs`  `mongodb` 编写的一套内容管理系统，结构简单，较目前一些开源的 `CMS` ，`DoraCMS` 易于拓展，特别适合前端开发工程师做二次开发。

#### 软件架构
`Nodejs` `eggjs` `mongodb`


#### 安装教程

```
sudo dnf install mongodb
sudo dnf install doracms
```

#### 启动
```
mongod &
service doracms start
```

#### 使用说明

1.  浏览器访问 http://127.0.0.1:8080
2.  后台管理  http://127.0.0.1:8080/dr-admin

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_doracms 分支
3.  提交代码
4.  新建 Pull Request


