# DoraCMS

#### Description
`DoraCMS` is a set of content management system based on `Nodejs` `eggjs`  `mongodb`. Its structure is simple. Compared with some open-source CMS, doracms is easy to expand, especially suitable for front-end development engineers to do secondary development.

#### Software Architecture
`Nodejs` `eggjs` `mongodb`

#### Installation

```
sudo dnf install mongodb
sudo dnf install doracms
```

#### Start
```
mongod &
service doracms start
```

#### Instructions

1.  Browser access http://127.0.0.1:8080
2.  Back-stage management  http://127.0.0.1:8080/dr-admin

#### Contribution

1.  Fork the repository
2.  Create Feat_doracms branch
3.  Commit your code
4.  Create Pull Request

